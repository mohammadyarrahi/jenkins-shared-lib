#!/usr/bin/env groovy

package com.example

class Docker implements Serializable {

    def script

    Docker(script) {
        this.script = script
    }

    def buildDockerImage(String IMAGE_NAME) {
        script.echo "building the docker image..."
        script.sh "docker build -t yarrahi/myapp:${IMAGE_NAME} . "
    }

    def dockerLogin() {
        script.withCredentials([script.usernamePassword(credentialsId: 'dockerhub-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]){
        script.echo "Using username: \${USER}"
        script.sh "echo \${PASS} | docker login -u \${USER} --password-stdin"
        }
    }

    def dockerPush(String IMAGE_NAME) {
        script.sh "docker push yarrahi/myapp:${IMAGE_NAME}"
    }
}
