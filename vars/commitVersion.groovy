#!/usr/bin/env groovy

def call() {
    
     withCredentials([gitUsernamePassword(credentialsId: 'gitlab-credentials', gitToolName: 'git-tool' , usernameVariable: 'USER', passwordVariable: 'PASS')]) {
        sh 'git config --global user.email "jenkins@example.com"'
        sh 'git config --global user.name "jenkins"'
        sh "git remote set-url origin https://gitlab.com/mohammadyarrahi/bootcamp_jenkins.git"
        sh 'git add .'
        sh 'git commit -m "ci: version bump"'
        sh 'git push origin HEAD:refs/heads/master'
    }
}

