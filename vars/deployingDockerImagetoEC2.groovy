#!/usr/bin/env groovy

def call(String IMAGE_NAME) {
    
    def shellCmd = "bash ./server-cmds.sh yarrahi/myapp:${IMAGE_NAME}"
    def ec2Instance = "ec2-user@3.70.25.182"

    sshagent(['ec2-server-key']) {
        sh "scp -o StrictHostKeyChecking=no server-cmds.sh ${ec2Instance}:/home/ec2-user"
        sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/ec2-user"
        sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
    }   
}
